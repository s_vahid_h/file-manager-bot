import os

basedir = os.path.abspath(os.path.dirname(__file__))
TOKEN = "BOT TOKEN"

class Config(object):
    URL = f"https://api.telegram.org/bot{TOKEN}/"